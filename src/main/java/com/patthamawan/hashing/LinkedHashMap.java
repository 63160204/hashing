/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.patthamawan.hashing;
import java.util.LinkedHashMap;
/**
 *
 * @author Oil
 */
public class LinkedHashMap {

    public static void main(String a[]) {
        LinkedHashMap<String, String> lhm = new LinkedHashMap<String, String>();

        lhm.put("one", "practice.Frame.org");
        lhm.put("two", "code.Oil.org");
        lhm.put("four", "quiz.Mark.org");

        System.out.println(lhm);

        System.out.println("Getting value for key 'one': "
                + lhm.get("one"));
        System.out.println("Size of the map: " + lhm.size());
        System.out.println("Is map empty? " + lhm.isEmpty());
        System.out.println("Contains key 'two'? "
                + lhm.containsKey("two"));
        System.out.println("Contains value 'Frame" + "frammy'?" + lhm.containsValue("practice" + "Frame.org"));
        System.out.println("delete element 'one': "
                + lhm.remove("one"));
        System.out.println(lhm);

    }
}
