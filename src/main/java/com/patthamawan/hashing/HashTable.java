/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.patthamawan.hashing;

import java.util.Hashtable;

/**
 *
 * @author Oil
 */
public class HashTable {

    public static void main(String args[]) {
        Hashtable<Integer, String> hm = new Hashtable<Integer, String>();
        hm.put(1, "Frame");
        hm.put(12, "Oil");
        hm.put(15, "Mark");
        hm.put(3, "Frammy");

        System.out.println(hm);
    }
}
