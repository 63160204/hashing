/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.patthamawan.hashing;

import java.util.HashMap;

/**
 *
 * @author Oil
 */
public class HashMap {

    public static void main(String[] args) {
        // Create a HashMap object called capitalCities
        HashMap<String, String> capitalCities = new HashMap<String, String>();

        capitalCities.put("Lao", "Veangchan");
        capitalCities.put("Khumphucha", "Phanomphan");
        capitalCities.put("Vietnam", "Hanoy");
        capitalCities.put("Thailand", "Bangkok");
        System.out.println(capitalCities);
    }
}
